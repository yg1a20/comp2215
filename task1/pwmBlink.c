#include "pico/stdlib.h"
#include <stdio.h>
#include "pico/time.h"
#include "hardware/irq.h"
#include "hardware/pwm.h"

void on_pwm_wrap() {
    static int fade = 0;
    static bool going_up = true;
    // Clear the interrupt flag that brought us here
    pwm_clear_irq(pwm_gpio_to_slice_num(PICO_DEFAULT_LED_PIN));

    if (going_up) {
        ++fade;
        if (fade > 255) {
            fade = 255;
            going_up = false;
        }
    } else {
        --fade;
        if (fade < 0) {
            fade = 0;
            going_up = true;
        }
    }
    // Square the fade value to make the LED's brightness appear more linear
    // Note this range matches with the wrap value
 
}

int main() {
    // Tell the LED pin that the PWM is in charge of its value.
   
    // Figure out which slice we just connected to the LED pin
    

    

    // counter is allowed to wrap over its maximum range (0 to 2**16-1)
   
    // Set divider, reduces counter clock to sysclock/this value
    
    // Load the configuration into our PWM slice, and set it running.
   
   
}
